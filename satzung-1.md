                   
# Satzung des Fab City Hamburg e.V.

## § 1 Name, Sitz, Eintragung, Geschäftsjahr
(1) Der Verein trägt den Namen Fab City Hamburg Förderverein e.V.

(2) Er hat den Sitz in Hamburg.

(3) Er soll in das Vereinsregister eingetragen werden.

(4) Geschäftsjahr ist das Kalenderjahr.

## § 2 Vereinszweck

(1) Der Verein verfolgt ausschließlich und unmittelbar gemeinnützige Zwecke im Sinne des Abschnitts "Steuerbegünstigte Zwecke" der Abgabenordnung (§§ 51ff) in der jeweils gültigen Fassung. 

(2) Zweck des Vereins ist die Förderung von Wissenschaft und Forschung und die Förderung der Volks- und Berufsbildung einschließlich der Studentenhilfe. Der Satzungszweck wird insbesondere verwirklicht durch Vernetzung der Mitglieder, Interessenvertretung der Mitglieder im Sinne der Commons auf städtischer, nationaler und internationaler Ebene und Unterstützung offener Werkstätten mit digitaler Fabrikation.

## § 3 Selbstlosigkeit
Der Verein verfolgt ausschließlich und unmittelbar – gemeinnützige – mildtätige – kirchliche – Zwecke (nicht verfolgte Zwecke streichen) im Sinne des Abschnitts "Steuerbegünstigte Zwecke "" der Abgabenordnung.
Der Verein ist selbstlos tätig; er verfolgt nicht in erster Linie eigenwirtschaftliche Zwecke.
Mittel des Vereins dürfen nur für die satzungsmäßigen Zwecke verwendet werden. Die Mitglieder erhalten in ihrer Eigenschaft als Mitglieder keine Zuwendungen aus Mitteln des Vereins.
Es darf keine Person durch Ausgaben, die dem Zweck der Körperschaft fremd sind, oder durch unverhältnismäßig hohe Vergütungen begünstigt werden.

## § 4 Mitgliedschaft
(1) Mitglied des Vereins kann jede natürliche (und juristische) Person werden, die seine Ziele unterstützt.

(2) Über den Antrag auf Aufnahme in den Verein entscheidet der Vorstand.

(3) Die Mitgliedschaft endet durch Austritt, Ausschluss oder Tod.

(4) Der Austritt eines Mitgliedes ist nur zum ........ möglich. Er erfolgt durch schriftliche Erklärung gegenüber dem Vorsitzenden unter Einhaltung einer Frist von ........

(5) Wenn ein Mitglied gegen die Ziele und Interessen des Vereins schwer verstoßen hat oder trotz Mahnung mit dem Beitrag für ......... Monate im Rückstand bleibt, so kann es durch den Vorstand mit sofortiger Wirkung ausgeschlossen werden.
Dem Mitglied muss vor der Beschlussfassung Gelegenheit zur Rechtfertigung bzw. Stellungnahme gegeben werden.
Gegen den Ausschließungsbeschluss kann innerhalb einer Frist von ..................... nach Mitteilung des Ausschlusses Berufung eingelegt werden, über den die nächste Mitgliederversammlung entscheidet.

## § 5 Beiträge
Die Mitglieder zahlen Beiträge nach Maßgabe eines Beschlusses der Mitgliederversammlung. Zur Festlegung der Beitragshöhe und -fälligkeit ist eine einfache Mehrheit der in der Mitgliederversammlung anwesenden stimmberechtigten Vereinsmitglieder erforderlich.

## § 6 Organe des Vereins
Organe des Vereins sind

a) der Vorstand

b) die Mitgliederversammlung

## § 7 Der Vorstand
(1) Der Vorstand besteht aus .... Mitgliedern
Er vertritt den Verein gerichtlich und außergerichtlich. Je zwei Vorstandsmitglieder sind gemeinsam vertretungsberechtigt.

(2) Der Vorstand wird von der Mitgliederversammlung für die Dauer von .... Jahren gewählt.
Die Wiederwahl der Vorstandsmitglieder ist möglich.
Der Vorsitzende wird von der Mitgliederversammlung in einem besonderen Wahlgang bestimmt. Die jeweils amtierenden Vorstandsmitglieder bleiben nach Ablauf ihrer Amtszeit im Amt, bis Nachfolger gewählt sind.

(3) Dem Vorstand obliegt die Führung der laufenden Geschäfte des Vereins. Er hat insbesondere folgende Aufgaben: Der Vorstand kann für die Geschäfte der laufenden Verwaltung einen Geschäftsführer bestellen. Dieser ist berechtigt, an den Sitzungen des Vorstandes mit beratender Stimme teilzunehmen.

(4) Vorstandssitzungen finden jährlich mindestens ...mal statt. Die Einladung zu Vorstandssitzungen erfolgt durch ........... schriftlich unter Einhaltung einer Einladungsfrist von mindestens ..... Tagen.

(5) Der Vorstand fasst seine Beschlüsse mit ...............(einfacher?) Mehrheit.

(6) Beschlüsse des Vorstands können bei Eilbedürftigkeit auch schriftlich oder fernmündlich gefasst werden, wenn alle Vorstandsmitglieder ihre Zustimmung zu diesem Verfahren schriftlich oder fernmündlich erklären. Schriftlich oder fernmündlich gefasste Vorstandsbeschlüsse sind schriftlich niederzulegen und von zu unterzeichnen.

(7) Der Vorstand kann für seine Tätigkeit eine angemessene Vergütung erhalten.

## § 8 Mitgliederversammlung
(1) Die Mitgliederversammlung ist einmal jährlich einzuberufen.

(2) Eine außerordentliche Mitgliederversammlung ist einzuberufen, wenn es das Vereinsinteresse erfordert oder wenn die Einberufung von ...... der Vereinsmitglieder schriftlich und unter Angabe des Zweckes und der Gründe verlangt wird.

(3) Die Einberufung der Mitgliederversammlung erfolgt schriftlich durch ................. unter Wahrung einer Einladungsfrist von mindestens ........ Wochen bei gleichzeitiger Bekanntgabe der Tagesordnung. Die Frist beginnt mit dem auf die Absendung des Einladungsschreibens folgenden Tag. Es gilt das Datum des Poststempels. Das Einladungsschreiben gilt dem Mitglied als zugegangen, wenn es an die letzte vom Mitglied des Vereins schriftlich bekannt gegebene Adresse gerichtet ist.

(4) Die Mitgliederversammlung als das oberste beschlussfassende Vereinsorgan ist grundsätzlich für alle Aufgaben zuständig, sofern bestimmte Aufgaben gemäß dieser Satzung nicht einem anderen Vereinsorgan übertragen wurden.
Ihr sind insbesondere die Jahresrechnung und der Jahresbericht zur Beschlussfassung über die Genehmigung und die Entlastung des Vorstandes schriftlich vorzulegen. Sie bestellt zwei Rechnungsprüfer, die weder dem Vorstand noch einem vom Vorstand berufenen Gremium angehören und auch nicht Angestellte des Vereins sein dürfen, um die Buchführung einschließlich Jahresabschluss zu prüfen und über das Ergebnis vor der Mitgliederversammlung zu berichten.

Die Mitgliederversammlung entscheidet z. B. auch über

a) Gebührenbefreiungen,

b) Aufgaben des Vereins,

c) An- und Verkauf sowie Belastung von Grundbesitz,

d) Beteiligung an Gesellschaften,

e) Aufnahme von Darlehen ab EUR ...........,

f) Genehmigung aller Geschäftsordnungen für den Vereinsbereich,

g) Mitgliedsbeiträge,

h) Satzungsänderungen,

i) Auflösung des Vereins.

(5) Jede satzungsmäßig einberufene Mitgliederversammlung wird als beschlussfähig anerkannt ohne Rücksicht auf die Zahl der erschienenen Vereinsmitglieder. Jedes Mitglied hat eine Stimme.

(6) Die Mitgliederversammlung fasst ihre Beschlüsse mit einfacher Mehrheit. Bei Stimmengleichheit gilt ein Antrag als abgelehnt.

## § 9 Aufwandsersatz
(1) Mitglieder – soweit sie vom Vorstand beauftragt wurden – und Vorstandsmitglieder haben einen Anspruch auf Ersatz der Aufwendungen, die ihnen im Rahmen ihrer Tätigkeit für den Verein entstanden sind. Dazu gehören insbesondere Reisekosten, Verpflegungsmehraufwendungen, Porto und Kommunikationskosten.

(2) Der Nachweis erfolgt über entsprechende Einzelbelege und ist spätestens 6 Wochen nach Ende des jeweiligen Quartals geltend zu machen.

(3) Soweit für den Aufwandsersatz steuerliche Pauschalen und steuerfreie Höchstgrenzen bestehen, erfolgt ein Ersatz nur in dieser Höhe.

## § 10 Satzungsänderung
(1) Für Satzungsänderungen ist eine ................-Mehrheit der erschienenen Vereinsmitglieder erforderlich. Für Änderungen des Satzungszwecks ist eine Mehrheit von ... der erschienenen Vereinsmitglieder erforderlich. Über Satzungsänderungen kann in der Mitgliederversammlung nur abgestimmt werden, wenn auf diesen Tagesordnungspunkt bereits in der Einladung zur Mitgliederversammlung hingewiesen wurde und der Einladung sowohl der bisherige als auch der vorgesehene neue Satzungstext beigefügt worden waren.

(2) Satzungsänderungen, die von Aufsichts-, Gerichts- oder Finanzbehörden aus formalen Gründen verlangt werden, kann der Vorstand von sich aus vornehmen. Diese Satzungsänderungen müssen allen Vereinsmitgliedern alsbald schriftlich mitgeteilt werden.

## § 11 Beurkundung von Beschlüssen
Die in Vorstandssitzungen und in Mitgliederversammlungen erfassten Beschlüsse sind schriftlich niederzulegen und vom Vorstand zu unterzeichnen.

## § 12 Auflösung des Vereins und Vermögensbindung
(1) Für den Beschluss, den Verein aufzulösen, ist eine 3/4-Mehrheit der in der Mitgliederversammlung anwesenden Mitglieder erforderlich. Der Beschluss kann nur nach rechtzeitiger Ankündigung in der Einladung zur Mitgliederversammlung gefasst werden.

(2) Bei Auflösung oder Aufhebung des Vereins oder bei Wegfall steuerbegünstigter Zwecke fällt das Vermögen des Vereins an ................... (Bezeichnung einer juristischen Person des öffentlichen Rechts oder einer anderen steuerbegünstigten Körperschaft)
- der - die - das - es unmittelbar und ausschließlich für gemeinnützige, mildtätige oder kirchliche Zwecke zu verwenden hat,

alternativ

b) an eine juristische Person des öffentlichen Rechts oder eine andere steuerbegünstigte Körperschaft zwecks Verwendung für ........... (Angabe eines bestimmten gemeinnützigen, mildtätigen oder kirchlichen Zwecks).



..........................................
(Ort) (Datum)


..........................................
(Unterschriften)


